import React, {useState, useCallback, useRef} from 'react';
import Activity from '../../components/common/Activity';
import {selectIsRequesting} from '../../store/request/request-selector';
import {selectStrings} from '../../store/localization/localization-selector';
import {useSelector} from 'react-redux';
import {
    Button,
    Icon,
    Input,
    Layout,
    StyleService,
    Text,
    TopNavigation,
    TopNavigationAction,
    useStyleSheet,
} from '@ui-kitten/components';
import {useDispatch} from 'react-redux';
import VideoAction from '../../store/video/video-action';
import Loader from '../../components/common/Loader';
import Modal from 'react-native-modal';
import VimeoVideo from '../../components/common/VimeoVideo';

const BackIcon = (props) => <Icon {...props} name="arrow-back" />;

const BackAction = ({onPress}) => (
    <TopNavigationAction onPress={onPress} icon={BackIcon} />
);

const INTERVAL = 10;

const VimeoVideoPlaybackScreen = (props) => {
    const isLoading = useSelector((state) =>
        selectIsRequesting(state, [
            VideoAction.REQUEST_FETCH_VIMEO_VIDEO_CONFIG,
        ]),
    );
    const strings = useSelector((state) => selectStrings(state));

    const dispatch = useDispatch();
    const fetchVimeoVideoConfig = useCallback(
        (vimeoUrl) => dispatch(VideoAction.fetchVimeoVideoConfig(vimeoUrl)),
        [dispatch],
    );
    const styles = useStyleSheet(themedStyles);
    const [url, setUrl] = useState();
    const [video, setVideo] = useState();
    const [playback, setPlayback] = useState({
        elapsedTime: 0,
        isShown: true,
        confirm: false,
    });
    const rnVideoRef = useRef();

    const {navigation} = props;

    function onPlayVideo() {
        fetchVimeoVideoConfig(url).then((response) => {
            if (response) {
                setVideo({
                    videoUrl:
                        response.data.request.files.hls.cdns[
                            response.data.request.files.hls.default_cdn
                        ].url,
                    video: response.data.video,
                });
            }
        });
    }

    return (
        <Activity
            toolbar={[
                <TopNavigation
                    key={1}
                    accessoryLeft={() => (
                        <BackAction onPress={() => navigation.goBack()} />
                    )}
                    title={(evaProps) => (
                        <Text {...evaProps}>
                            {video ? video.video.title : strings.playVideo}
                        </Text>
                    )}
                />,
            ]}>
            <Modal isVisible={playback.confirm}>
                <Layout style={[styles.confirmModal]}>
                    <Text category={'h6'}>{strings.continueVideo}</Text>
                    <Layout style={[styles.actions]}>
                        <Button
                            style={[styles.mr4]}
                            onPress={() => navigation.goBack()}
                            size={'small'}>
                            {strings.no}
                        </Button>
                        <Button
                            onPress={() => {
                                setPlayback((s) => {
                                    return {
                                        ...s,
                                        confirm: false,
                                    };
                                });
                                rnVideoRef.current.handlePlayPause();
                            }}
                            size={'small'}>
                            {strings.yes}
                        </Button>
                    </Layout>
                </Layout>
            </Modal>
            <Layout style={[styles.container]}>
                <Layout style={[styles.searchBar]}>
                    <Input
                        style={[styles.f1, styles.mr4, styles.urlInput]}
                        placeholder={strings.url}
                        value={url}
                        onChangeText={(nextValue) => setUrl(nextValue)}
                    />
                    <Button
                        onPress={onPlayVideo}
                        style={[styles.playButton]}
                        size={'small'}>
                        {strings.play}
                    </Button>
                </Layout>
                {video && !isLoading && (
                    <Layout style={[styles.vimeoVideoContainer]}>
                        <VimeoVideo src={url} />
                    </Layout>
                )}

                {isLoading && <Loader isLoading={isLoading} />}
            </Layout>
        </Activity>
    );
};

const themedStyles = StyleService.create({
    container: {
        flex: 1,
    },
    f1: {
        flex: 1,
    },
    mr4: {
        marginRight: 4,
    },
    playButton: {
        height: 40,
    },
    searchBar: {
        display: 'flex',
        flexDirection: 'row',
        padding: 12,
    },
    confirmModal: {
        padding: 12,
        // flex: 1,
    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 12,
    },
    vimeoVideoContainer: {
        height: 200,
    },
});

export default VimeoVideoPlaybackScreen;
