import React from 'react';
import Activity from '../../components/common/Activity';
import {selectIsRequesting} from '../../store/request/request-selector';
import {selectStrings} from '../../store/localization/localization-selector';
import {useSelector} from 'react-redux';
import {
    Button,
    Layout,
    StyleService,
    useStyleSheet,
} from '@ui-kitten/components';
import ScreenEnum from '../../constants/ScreenEnum';

const DashBoardScreen = (props) => {
    const isLoading = useSelector((state) => selectIsRequesting(state, []));
    const strings = useSelector((state) => selectStrings(state));

    const styles = useStyleSheet(themedStyles);

    const {navigation} = props;

    return (
        <Activity isLoading={isLoading}>
            <Layout style={[styles.layout]}>
                <Button
                    style={[styles.mb12]}
                    onPress={() =>
                        navigation.navigate(ScreenEnum.VIDEO_PLAYBACK)
                    }>
                    {strings.playVideo}
                </Button>
                <Button
                    onPress={() =>
                        navigation.navigate(ScreenEnum.VIMEO_VIDEO_PLAYBACK)
                    }>
                    {strings.playVimeoVideo}
                </Button>
                <Button
                    onPress={() =>
                        navigation.navigate(ScreenEnum.GAME_DASHBOARD)
                    }>
                    {strings.playGame}
                </Button>
            </Layout>
        </Activity>
    );
};

const themedStyles = StyleService.create({
    layout: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        padding: 16,
    },
    mb12: {
        marginBottom: 12,
    },
});

export default DashBoardScreen;
