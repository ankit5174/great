import React, {useCallback} from 'react';
import Activity from '../../components/common/Activity';
import {selectIsRequesting} from '../../store/request/request-selector';
import {selectStrings} from '../../store/localization/localization-selector';
import {useDispatch, useSelector} from 'react-redux';
import {
    Button,
    Input,
    Layout,
    StyleService,
    useStyleSheet,
} from '@ui-kitten/components';
import GameAction from '../../store/game/game-action';
import {selectGameGrid} from '../../store/game/game-selector';
import ScreenEnum from '../../constants/ScreenEnum';

const GameDashBoardScreen = (props) => {
    const isLoading = useSelector((state) => selectIsRequesting(state, []));
    const strings = useSelector((state) => selectStrings(state));
    const gridSize = useSelector((state) => selectGameGrid(state));

    const dispatch = useDispatch();
    const setGameGrid = useCallback(
        (gridSize) => dispatch(GameAction.setGameGrid(gridSize)),
        [dispatch],
    );

    const styles = useStyleSheet(themedStyles);

    const {navigation} = props;

    const onGridSizeChange = (value) => {
        setGameGrid(value);
    };

    return (
        <Activity isLoading={isLoading}>
            <Layout style={[styles.layout]}>
                <Input
                    placeholder={strings.gridSize}
                    value={gridSize}
                    onChangeText={onGridSizeChange}
                />
                <Button onPress={() => navigation.navigate(ScreenEnum.GAME)}>
                    {strings.startGame}
                </Button>
            </Layout>
        </Activity>
    );
};

const themedStyles = StyleService.create({
    layout: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        padding: 16,
    },
});

export default GameDashBoardScreen;
