import React, {useEffect, useRef, useState} from 'react';
import Activity from '../../components/common/Activity';
import {selectIsRequesting} from '../../store/request/request-selector';
import {selectStrings} from '../../store/localization/localization-selector';
import {useSelector} from 'react-redux';
import {
    Button,
    Layout,
    StyleService,
    Text,
    useStyleSheet,
} from '@ui-kitten/components';
import {selectGameGrid} from '../../store/game/game-selector';
import {Col, Grid, Row} from 'react-native-easy-grid';
import {TouchableHighlight} from 'react-native';
import {_checkDiagonals} from '../../util/CommonUtils';

const initialState = () => ({
    player: 0,
    gridValues: {},
});

const keysInitialValue = () => ({x: {}, y: {}});

const GameScreen = (props) => {
    const isLoading = useSelector((state) => selectIsRequesting(state, []));
    const strings = useSelector((state) => selectStrings(state));
    const gridSize = useSelector((state) => selectGameGrid(state));

    const styles = useStyleSheet(themedStyles);
    const [state, setState] = useState(initialState());
    const [winner, setWinner] = useState('');
    const keys = useRef(keysInitialValue());

    useEffect(() => {
        if (_checkDiagonals(state.gridValues, gridSize)) {
            setWinner(state.player === 1 ? 0 : 1);
        }
    }, [state.gridValues]);

    const {navigation} = props;

    const onCellPress = (cell) => {
        setState((prevState) => {
            if (prevState.gridValues[cell] === undefined && winner === '') {
                return {
                    player: prevState.player === 1 ? 0 : 1,
                    gridValues: {
                        ...prevState.gridValues,
                        [cell]: prevState.player,
                    },
                };
            } else {
                return prevState;
            }
        });
        if (state.gridValues[cell] === undefined && winner === '') {
            let [row, col] = cell.split(',');

            if (keys.current.x[row]) {
                if (keys.current.x[row].value === state.player) {
                    keys.current.x[row].count += 1;
                }
                if (keys.current.x[row].count === gridSize) {
                    setWinner(state.player);
                }
            } else {
                keys.current.x[row] = {};
                keys.current.x[row].value = state.player;
                keys.current.x[row].count = 1;
            }

            if (keys.current.y[col]) {
                if (keys.current.y[col].value === state.player) {
                    keys.current.y[col].count += 1;
                }
                if (keys.current.y[col].count === gridSize) {
                    setWinner(state.player);
                }
            } else {
                keys.current.y[col] = {};
                keys.current.y[col].value = state.player;
                keys.current.y[col].count = 1;
            }
        }
    };

    const onReset = () => {
        setState(initialState());
        setWinner('');
        keys.current = keysInitialValue();
    };

    return (
        <Activity isLoading={isLoading}>
            <Text>{`${strings.playerTurn} ${state.player}`}</Text>
            {gridSize ? (
                <Grid>
                    {gridSize &&
                        Array.from(Array(Number(gridSize))).map(
                            (value, row) => {
                                return (
                                    <Row style={{height: 25}}>
                                        {Array.from(
                                            Array(Number(gridSize)),
                                        ).map((value2, col) => {
                                            return (
                                                <Col>
                                                    <TouchableHighlight
                                                        onPress={() =>
                                                            onCellPress(
                                                                `${row},${col}`,
                                                            )
                                                        }>
                                                        <Layout
                                                            style={[
                                                                styles.cell,
                                                            ]}>
                                                            <Text>
                                                                {
                                                                    state
                                                                        .gridValues[
                                                                        `${row},${col}`
                                                                    ]
                                                                }
                                                            </Text>
                                                        </Layout>
                                                    </TouchableHighlight>
                                                </Col>
                                            );
                                        })}
                                    </Row>
                                );
                            },
                        )}
                </Grid>
            ) : (
                <></>
            )}
            {winner !== '' ? <Text>{strings.winner(winner)}</Text> : <></>}
            <Button onPress={onReset}>
                {winner !== '' ? strings.playAgain : strings.reset}
            </Button>
        </Activity>
    );
};

const themedStyles = StyleService.create({
    layout: {
        flex: 1,
        padding: 16,
    },
    cell: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        borderWidth: 1,
        borderColor: 'blue',
    },
});

export default GameScreen;
