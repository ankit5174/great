export const selectLanguage = (local) => {
    return strings[local];
};

const strings = {
    en: {
        gridSize: 'Grid Size',
        startGame: 'Start Game',
        playGame: 'Play Game',
        playVideo: 'Play Video',
        playVimeoVideo: 'Play Vimeo Video',
        playerTurn: 'Player Turn',
        reset: 'Reset',
        winner: (winner) => `Player ${winner} is the winner`,
        playAgain: 'Play Again',
        url: 'URL',
        play: 'Play',
        continueVideo: 'Continue Watching Video',
        no: 'NO',
        yes: 'YES',
    },
    ben: {},
};
