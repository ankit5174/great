import 'react-native-gesture-handler';
import React, {useState} from 'react';
import {getStore} from './store/configureStore';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ScreenEnum from './constants/ScreenEnum';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import GameDashboardScreen from './screens/GameDashboardScreen';
import GameScreen from './screens/GameScreen';
import DashboardScreen from './screens/DashboardScreen';
import VideoPlaybackScreen from './screens/VideoPlaybackScreen';
import VimeoVideoPlaybackScreen from './screens/VimeoVideoPlaybackScreen';
import {EvaIconsPack} from '@ui-kitten/eva-icons';

const Root = createStackNavigator();

const store = getStore({});

export const ThemeContext = React.createContext({
    theme: 'light',
    toggleTheme: () => {},
});

const App = () => {
    const [theme, setTheme] = useState('light');

    const toggleTheme = () => {
        const nextTheme = theme === 'light' ? 'dark' : 'light';
        setTheme(nextTheme);
    };

    return (
        <Provider store={store}>
            <IconRegistry icons={[EvaIconsPack]} />
            <ThemeContext.Provider value={{theme, toggleTheme}}>
                <ApplicationProvider {...eva} theme={eva[theme]}>
                    <SafeAreaProvider>
                        <NavigationContainer>
                            <Root.Navigator
                                initialRouteName={
                                    ScreenEnum.VIMEO_VIDEO_PLAYBACK
                                }
                                headerMode="none">
                                <Root.Screen
                                    name={ScreenEnum.GAME_DASHBOARD}
                                    component={GameDashboardScreen}
                                />
                                <Root.Screen
                                    name={ScreenEnum.VIMEO_VIDEO_PLAYBACK}
                                    component={VimeoVideoPlaybackScreen}
                                />
                                <Root.Screen
                                    name={ScreenEnum.GAME}
                                    component={GameScreen}
                                />
                                <Root.Screen
                                    name={ScreenEnum.VIDEO_PLAYBACK}
                                    component={VideoPlaybackScreen}
                                />
                                <Root.Screen
                                    name={ScreenEnum.DASHBOARD}
                                    component={DashboardScreen}
                                />
                            </Root.Navigator>
                        </NavigationContainer>
                    </SafeAreaProvider>
                </ApplicationProvider>
            </ThemeContext.Provider>
        </Provider>
    );
};

export default App;
