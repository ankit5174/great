const AppEnum = {
    PLATFORM_IOS: 'ios',
    PLATFORM_ANDROID: 'android',
};

export default AppEnum;
