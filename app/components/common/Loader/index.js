import React from 'react';
import {Modal, Spinner} from '@ui-kitten/components';

const Loader = (props) => {
    const {isLoading} = props;
    return (
        <Modal visible={isLoading}>
            <Spinner size='giant'/>
        </Modal>
    );
};

export default Loader;
