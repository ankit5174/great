import React, {useEffect, useState, useRef} from 'react';
import {StyleService, useStyleSheet} from '@ui-kitten/components';
import {WebView} from 'react-native-webview';

const createVideoHtml = (url) =>
    `<!DOCTYPE html>
     <html lang="en">
        <head>
            <title></title>
            <script type="text/javascript" src="https://player.vimeo.com/api/player.js"/>
        </head>
        <body>
        <h1>Ankit</h1>
            <iframe width="100%" height="100%" src=${url} allowfullscreen />
        </body>
    </html>`;

const VimeoVideo = ({src}) => {
    const styles = useStyleSheet(themedStyles);
    const [state, setState] = useState({});
    const webViewRef = useRef();

    useEffect(() => {
        setState((s) => ({...s, htmlSrc: createVideoHtml(src)}));
    }, [src]);

    function injectJSFileFromWeb() {
        const jsFileName = 'https://player.vimeo.com/api/player.js';
        const fp = `
        var vimeoScript = document.createElement('script');
        vimeoScript.type = 'text/javascript';
        vimeoScript.src = "${jsFileName}";
        var parent = document.getElementsByTagName('head').item(0);
        parent.appendChild(vimeoScript);
        vimeoScript.onload = function() {
            var iframe = document.querySelector('iframe');
            var player = new Vimeo.Player(iframe);
             
            player.getVideoTitle().then(function(title) {
                console.log('title:', title);
            });
            
            player.on('timeupdate', function (data) {
            window.ReactNativeWebView.postMessage(
                JSON.stringify({
                    type: 'timeupdate',
                    payload: data,
                }),
            );
        });
        } 
        void(0);
        `;
        webViewRef.current.injectJavaScript(fp);
    }

    return state.htmlSrc ? (
        <WebView
            ref={webViewRef}
            style={{
                height: 200,
                alignSelf: 'stretch',
                backgroundColor: 'grey',
            }}
            javaScriptEnabled={true}
            originWhitelist={['*']}
            source={{
                html: `<iframe id="vimeo-video" width="100%" height="100%" src=${src} allowfullscreen />`,
            }}
            scrollEnabled={false}
            scalesPageToFit={true}
            domStorageEnabled={true}
            onLoad={() => injectJSFileFromWeb()}
            onMessage={({nativeEvent}) => {
                const event = JSON.parse(nativeEvent.data);
                switch (event.type) {
                    case 'timeupdate':
                        console.log(event.payload);
                        break;
                    default:
                        break;
                }
            }}
        />
    ) : (
        <></>
    );
};

const themedStyles = StyleService.create({});

export default VimeoVideo;
