import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import DefaultLoader from '../Loader';
import {
    Divider,
    Layout,
    StyleService,
    useStyleSheet,
} from '@ui-kitten/components';

const Activity = (props) => {
    const styles = useStyleSheet(themedStyles);

    const {
        isLoading,
        level,
        children,
        loaderComponent: Loader,
        toolbar,
        layoutStyle = [],
        bottomToolbar,
    } = props;

    return (
        <SafeAreaView style={styles.container}>
            {toolbar}
            <Divider />
            <Layout
                level={level || '1'}
                style={[styles.layout, ...layoutStyle]}>
                {Loader ? (
                    <Loader isLoading={isLoading} />
                ) : (
                    <DefaultLoader isLoading={isLoading} />
                )}
                {children}
            </Layout>
            {bottomToolbar}
        </SafeAreaView>
    );
};

const themedStyles = StyleService.create({
    container: {
        flex: 1,
    },
    layout: {
        display: 'flex',
        flex: 1,
    },
});

export default Activity;
