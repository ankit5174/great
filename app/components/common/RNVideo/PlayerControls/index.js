import React from 'react';
import {StyleService, useStyleSheet} from '@ui-kitten/components';
import {TouchableOpacity, View} from 'react-native';
import VideoPause from '../../../../assets/icons/video-pause.svg';
import VideoPlay from '../../../../assets/icons/video-play.svg';
import VideoBackward from '../../../../assets/icons/video-backward.svg';
import VideoForward from '../../../../assets/icons/video-forward.svg';

const PlayerControls = (props) => {
    const styles = useStyleSheet(themedStyles);

    const {
        playing,
        onPlay,
        onPause,
        skipBackward,
        skipForward,
        showSkip,
    } = props;

    return (
        <View style={[styles.container]}>
            {showSkip && (
                <TouchableOpacity
                    style={styles.touchable}
                    onPress={skipBackward}>
                    <VideoBackward />
                </TouchableOpacity>
            )}
            <TouchableOpacity
                style={styles.touchable}
                onPress={playing ? onPause : onPlay}>
                {playing ? <VideoPause /> : <VideoPlay />}
            </TouchableOpacity>
            {showSkip && (
                <TouchableOpacity
                    style={styles.touchable}
                    onPress={skipForward}>
                    <VideoForward />
                </TouchableOpacity>
            )}
        </View>
    );
};

const themedStyles = StyleService.create({
    container: {
        paddingHorizontal: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flex: 3,
        // backgroundColor: '#000000c4',
    },
    video: {
        alignSelf: 'stretch',
        backgroundColor: 'black',
    },
});

export default PlayerControls;
