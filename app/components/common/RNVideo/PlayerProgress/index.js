import React from 'react';
import {StyleService, useStyleSheet} from '@ui-kitten/components';
import {Text, View} from 'react-native';
import Slider from '@react-native-community/slider';
import {_getMinutesFromSeconds} from '../../../../util/CommonUtils';

const PlayerProgress = (props) => {
    const styles = useStyleSheet(themedStyles);

    const {
        currentTime,
        duration,
        onSlideStart,
        onSlideComplete,
        onSlideCapture,
    } = props;

    const position = _getMinutesFromSeconds(currentTime);
    const fullDuration = _getMinutesFromSeconds(duration);

    function handleOnSlide(time) {
        onSlideCapture({seekTime: time});
    }

    return (
        <View style={styles.wrapper}>
            <Slider
                value={currentTime}
                minimumValue={0}
                maximumValue={duration}
                step={1}
                onValueChange={handleOnSlide}
                onSlidingStart={onSlideStart}
                onSlidingComplete={onSlideComplete}
                minimumTrackTintColor={'#F44336'}
                maximumTrackTintColor={'#FFFFFF'}
                thumbTintColor={'#F44336'}
            />
            <View style={styles.timeWrapper}>
                <Text style={styles.timeLeft}>{`${
                    position.minutes >= 10
                        ? position.minutes
                        : '0' + position.minutes
                }:${
                    position.seconds >= 10
                        ? position.seconds
                        : '0' + position.seconds
                }`}</Text>
                <Text style={styles.timeRight}>{`${
                    fullDuration.minutes >= 10
                        ? fullDuration.minutes
                        : '0' + fullDuration.minutes
                }:${
                    fullDuration.seconds >= 10
                        ? fullDuration.seconds
                        : '0' + fullDuration.seconds
                }`}</Text>
            </View>
        </View>
    );
};

const themedStyles = StyleService.create({
    container: {
        flex: 1,
        backgroundColor: 'blue',
    },
    timeWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 5,
        marginBottom: 8,
    },
    timeLeft: {
        flex: 1,
        fontSize: 12,
        color: '#FFFFFF',
        paddingLeft: 10,
    },
    timeRight: {
        flex: 1,
        fontSize: 12,
        color: '#FFFFFF',
        textAlign: 'right',
        paddingRight: 10,
    },
});

export default PlayerProgress;
