import React, {useEffect, useRef, useState} from 'react';
import {StyleService, useStyleSheet} from '@ui-kitten/components';
import Video from 'react-native-video';
import {
    Dimensions,
    StatusBar,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
} from 'react-native';
import Orientation from 'react-native-orientation-locker';
import FullscreenClose from '../../../assets/icons/fullscreen-close.svg';
import FullscreenOpen from '../../../assets/icons/fullscreen-open.svg';
import PlayerControls from './PlayerControls';
import PlayerProgress from './PlayerProgress';

const RNVideo = (props) => {
    const styles = useStyleSheet(themedStyles);
    const [state, setState] = useState({
        fullscreen: false,
        play: props.autoPlay || false,
        currentTime: 0,
        duration: 0,
        showControls: false,
    });
    const videoRef = useRef();

    useEffect(() => {
        Orientation.addOrientationListener(handleOrientation);

        return () => {
            Orientation.removeOrientationListener(handleOrientation);
        };
    }, []);

    useEffect(() => {
        props.playbackRef.current = {
            handlePlayPause,
        };
    }, []);

    const {
        src,
        styles: videoStyles = [],
        resizeMode = 'contain',
        height = Dimensions.get('window').width * (9 / 16),
        onProgressCallback,
        navigation,
    } = props;

    function handleOrientation(orientation) {
        orientation === 'LANDSCAPE-LEFT' || orientation === 'LANDSCAPE-RIGHT'
            ? (setState((s) => ({...s, fullscreen: true})),
              StatusBar.setHidden(true),
              navigation.setOptions({headerShown: false}))
            : (setState((s) => ({...s, fullscreen: false})),
              StatusBar.setHidden(false),
              navigation.setOptions({headerShown: true}));
    }

    function handleFullscreen() {
        state.fullscreen
            ? Orientation.lockToPortrait()
            : Orientation.lockToLandscapeLeft();
    }

    function handlePlayPause() {
        setState((s) => {
            if (!s.play) {
                setTimeout(
                    () => setState((s) => ({...s, showControls: false})),
                    2000,
                );
            }
            return {
                ...s,
                play: !s.play,
                showControls: s.play ? true : s.showControls,
            };
        });
    }

    function skipBackward() {
        videoRef.current.seek(state.currentTime - 15);
        setState({...state, currentTime: state.currentTime - 15});
    }

    function skipForward() {
        videoRef.current.seek(state.currentTime + 15);
        setState({...state, currentTime: state.currentTime + 15});
    }

    function onSeek(data) {
        videoRef.current.seek(data.seekTime);
        setState({...state, currentTime: data.seekTime});
    }

    function onLoadEnd(data) {
        setState((s) => ({
            ...s,
            duration: data.duration,
            currentTime: data.currentTime,
        }));
    }

    function onProgress(data) {
        setState((s) => ({
            ...s,
            currentTime: data.currentTime,
        }));
        if (onProgressCallback) {
            onProgressCallback(data);
        }
    }

    function onEnd() {
        setState({...state, play: false});
        videoRef.current.seek(0);
    }

    function showControls() {
        setState((s) => ({...s, showControls: !s.showControls}));
    }

    return (
        <TouchableWithoutFeedback onPress={showControls}>
            <View>
                <Video
                    ref={videoRef}
                    resizeMode={resizeMode}
                    source={{
                        uri: src,
                    }}
                    style={[
                        ...videoStyles,
                        state.fullscreen
                            ? styles.fullscreenVideo
                            : styles.video,
                        state.fullscreen ? [] : {height: height},
                    ]}
                    controls={false}
                    onLoad={onLoadEnd}
                    onProgress={onProgress}
                    onEnd={onEnd}
                    paused={!state.play}
                />

                {state.showControls && (
                    <View style={styles.controlOverlay}>
                        <TouchableOpacity
                            onPress={handleFullscreen}
                            // hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            style={styles.fullscreenButton}>
                            {state.fullscreen ? (
                                <FullscreenClose />
                            ) : (
                                <FullscreenOpen />
                            )}
                        </TouchableOpacity>
                        <PlayerControls
                            showSkip={true}
                            skipBackward={skipBackward}
                            skipForward={skipForward}
                            onPlay={handlePlayPause}
                            onPause={handlePlayPause}
                            playing={state.play}
                        />
                        <PlayerProgress
                            currentTime={state.currentTime}
                            duration={state.duration > 0 ? state.duration : 0}
                            onSlideStart={handlePlayPause}
                            onSlideComplete={handlePlayPause}
                            onSlideCapture={onSeek}
                        />
                    </View>
                )}
            </View>
        </TouchableWithoutFeedback>
    );
};

const themedStyles = StyleService.create({
    video: {
        alignSelf: 'stretch',
        backgroundColor: 'black',
    },
    fullscreenVideo: {
        height: Dimensions.get('window').width,
        width: Dimensions.get('window').height,
        backgroundColor: 'black',
    },
    controlOverlay: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'space-between',
        backgroundColor: '#000000c4',
    },
    fullscreenButton: {
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'center',
        paddingRight: 10,
    },
});

export default RNVideo;
