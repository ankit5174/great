import axios from 'axios';

const vimeoVideoConfigUrl = (vimeoUrl) => `${vimeoUrl}/config`;

//axios instance
export const request = axios.create({
    timeout: 60000,
    headers: {
        'content-type': 'application/json',
    },
});

export const _setRequestHeader = (header, value) => {
    request.defaults.headers.common[header] = value;
};

export const _setBaseURL = (baseURL) => {
    request.defaults.baseURL = baseURL;
};

export const _fetchVimeoVideoConfig = (vimeoUrl) => {
    return axios.get(vimeoVideoConfigUrl(vimeoUrl));
};
