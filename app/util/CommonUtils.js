export const _to = (promise) => {
    return promise
        .then((response) => [response, undefined])
        .catch((error) => Promise.resolve([undefined, error.response]));
};

export const _checkDiagonals = (gridValues, gridSize) => {
    let i = 0,
        j = 0;
    let x = gridValues[`${i},${j}`];
    while (
        i < gridSize &&
        gridValues[`${i},${j}`] === x &&
        gridValues[`${i},${j}`] !== undefined
    ) {
        i++;
        j++;
    }
    if (i === gridSize) {
        return true;
    }
    i = 0;
    j = gridSize - 1;
    x = gridValues[`${i},${j}`];
    while (
        i < gridSize &&
        j >= 0 &&
        gridValues[`${i},${j}`] === x &&
        gridValues[`${i},${j}`] !== undefined
    ) {
        i++;
        j--;
    }
    if (j === -1) {
        return true;
    }
};

export function _getMinutesFromSeconds(time) {
    const minutes = time >= 60 ? Math.floor(time / 60) : 0;
    const seconds = Math.floor(time - minutes * 60);

    return {
        minutes,
        seconds,
    };
}
