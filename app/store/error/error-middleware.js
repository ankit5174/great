import {selectStrings} from '../localization/localization-selector';

export default (store) => (next) => (action) => {
    if (action.error) {
        let {payload} = action;
        if (payload && !payload.data.errors) {
            let errorMessage =
                payload.statusText ||
                selectStrings(store.getState()).somethingWentWrong;
        }
    }
    next(action);
};
