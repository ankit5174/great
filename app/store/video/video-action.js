import ActionUtils from '../../util/ActionUtils';
import {_fetchVimeoVideoConfig} from '../../util/NetworkUtils';

export default class VideoAction {
    static REQUEST_FETCH_VIMEO_VIDEO_CONFIG =
        'REQUEST_FETCH_VIMEO_VIDEO_CONFIG';

    static fetchVimeoVideoConfig = (vimeoUrl) => {
        return async (dispatch) => {
            const [response] = await ActionUtils.createThunkEffect(
                dispatch,
                VideoAction.REQUEST_FETCH_VIMEO_VIDEO_CONFIG,
                _fetchVimeoVideoConfig,
                vimeoUrl,
            );
            return response;
        };
    };
}
