import ActionUtils from '../../util/ActionUtils';

export default class GameAction {
    static SET_GAME_GRID = 'SET_GAME_GRID';

    static setGameGrid = (gameGrid) => {
        return async (dispatch) => {
            dispatch(
                ActionUtils.createAction(GameAction.SET_GAME_GRID, gameGrid),
            );
        };
    };
}
