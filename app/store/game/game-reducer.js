import GameAction from './game-action';

const INITIAL_STATE = {
    grid: 3,
};

const onSetGameGrid = (state, action) => {
    return {
        ...state,
        grid: action.payload,
    };
};

export default (state = INITIAL_STATE, action) => {
    if (action.error) {
        return state;
    }
    switch (action.type) {
        case GameAction.SET_GAME_GRID:
            return onSetGameGrid(state, action);
        default:
            return state;
    }
};
