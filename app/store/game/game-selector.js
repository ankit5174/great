import {createSelector} from 'reselect';

export const selectGameGrid = createSelector(
    (state) => state.game,
    (game) => {
        return game.grid;
    },
);
