import {createSelector} from 'reselect';

export const selectStrings = createSelector(
    (state) => state.localization,
    (localization) => {
        return localization.strings;
    },
);
