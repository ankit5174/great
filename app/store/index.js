import {combineReducers} from 'redux';
import localizationReducer from './localization/localization-reducer';
import requestReducer from './request/request-reducer';
import errorReducer from './error/error-reducer';
import gameReducer from './game/game-reducer';

const appReducers = combineReducers({
    localization: localizationReducer,
    request: requestReducer,
    error: errorReducer,
    game: gameReducer,
});

const rootReducer = (state, action) => {
    switch (action.type) {
        default:
            return appReducers(state, action);
    }
};

export default rootReducer;
