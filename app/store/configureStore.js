import {applyMiddleware, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import {selectLanguage} from '../assets/strings';
import errorMiddleware from './error/error-middleware';

export let store = null;

export function getStore({locale}) {
    const composeEnhancers = compose;
    store = createStore(
        require('./index').default,
        {
            localization: {
                strings: selectLanguage(locale || 'en'),
            },
        },
        composeEnhancers(applyMiddleware(thunk, errorMiddleware)),
    );
    return store;
}
